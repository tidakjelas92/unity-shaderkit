namespace TidakJelas.ShaderKit
{
    public enum ShadingType
    {
        Lambert,
        Phong,
        BlinnPhong,
        HalfLambert,
        OrenNayar,
        Minnaert,
    }
}
