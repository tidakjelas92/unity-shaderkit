namespace TidakJelas.ShaderKit
{
    public enum DebugData
    {
        PositionOS,
        PositionWS,
        PositionCS,
        PositionVS,
        PositionNDC,
        VertexColor,
        UV,
        NormalOS,
        NormalWS,
        TangentOS,
        TangentWS,
        BitangentWS,
    }
}
