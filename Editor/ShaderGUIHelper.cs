using System;
using UnityEditor;
using UnityEngine;

namespace TidakJelas.ShaderKit
{
    public static class ShaderGUIHelper
    {
        public static bool CreateToggleControl(
            string propertyName,
            string label,
            MaterialProperty[] properties,
            bool required = true
        )
        {
            var property = BaseShaderGUI.FindProperty(propertyName, properties, required);

            property.floatValue = EditorGUILayout.Toggle(label, (int)property.floatValue == 1)
                ? 1.0f
                : 0.0f;

            return (int)property.floatValue == 1;
        }

        public static void CreateRangeControl(
            string propertyName,
            string label,
            MaterialProperty[] properties,
            bool required = true,
            float min = 0.0f,
            float max = 1.0f
        )
        {
            var property = BaseShaderGUI.FindProperty(propertyName, properties, required);
            property.floatValue = EditorGUILayout.Slider(label, property.floatValue, min, max);
        }

        public static void CreateColorControl(
            string propertyName,
            string label,
            MaterialProperty[] properties,
            bool required = true
        )
        {
            var property = BaseShaderGUI.FindProperty(propertyName, properties, required);
            property.colorValue = EditorGUILayout.ColorField(label, property.colorValue);
        }

        public static void CreateFloatControl(
            string propertyName,
            string label,
            MaterialProperty[] properties,
            bool required = true
        )
        {
            var property = BaseShaderGUI.FindProperty(propertyName, properties, required);
            property.floatValue = EditorGUILayout.FloatField(label, property.floatValue);
        }

        /// <summary>
        /// Depending on a propertyName float value, enable or disable passed keyword
        /// </summary>
        public static void UpdateKeyword(
            Material material,
            string propertyName,
            string keyword,
            Action<bool> additionalAction = null
        )
        {
            var enabled = (int)material.GetFloat(propertyName) == 1;
            if (enabled)
                material.EnableKeyword(keyword);
            else
                material.DisableKeyword(keyword);

            additionalAction?.Invoke(enabled);
        }
    }
}
