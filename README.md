# unity-shaderkit

Collection of reusable HLSL and other shader codes.

Current Unity version : Unity 2022.2.21f1

Compatibility with other Unity version is unknown, it is advisable to use the current Unity version this project is using or newer.
