#ifndef SHADERKIT_OPAQUE_DEPTH_ONLY_HLSL
#define SHADERKIT_OPAQUE_DEPTH_ONLY_HLSL

struct Attributes {
    float3 position_os : POSITION;
};

struct Varyings {
    float4 position_cs : SV_POSITION;
};

Varyings vertex(Attributes attributes) {
    Varyings varyings;

    VertexPositionInputs vertex_position_inputs = GetVertexPositionInputs(attributes.position_os);
    varyings.position_cs = vertex_position_inputs.positionCS;

    return varyings;
}

float4 fragment(Varyings varyings) : SV_TARGET {
    return varyings.position_cs.z;
}

#endif
