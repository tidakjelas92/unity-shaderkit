# Basic Shading

See this file first: `com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl`. It might be what you're looking for.

These functions offer a more granular approach, as it only returns the shading information
