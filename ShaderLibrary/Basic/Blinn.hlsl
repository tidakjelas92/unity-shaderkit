#ifndef SHADERKIT_BASIC_BLINN_HLSL
#define SHADERKIT_BASIC_BLINN_HLSL

float blinn(
    float3 light_direction_ws,
    float3 normal_ws,
    float3 view_direction_ws,
    float shininess
) {
    float3 half_direction = normalize(view_direction_ws + light_direction_ws);
    float result = max(dot(normal_ws, half_direction), 0);

    return pow(result, shininess);
}

#endif
