#ifndef SHADERKIT_BASIC_LAMBERT_HLSL
#define SHADERKIT_BASIC_LAMBERT_HLSL

float lambert(float3 light_direction_ws, float3 normal_ws) {
    return saturate(dot(light_direction_ws, normal_ws));
}

#endif
