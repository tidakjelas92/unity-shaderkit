#ifndef SHADERKIT_BASIC_HALF_LAMBERT_HLSL
#define SHADERKIT_BASIC_HALF_LAMBERT_HLSL

// source: https://steamcdn-a.akamaihd.net/apps/valve/2007/NPAR07_IllustrativeRenderingInTeamFortress2.pdf
// technique developed by valve for tf2.

// wrap default value is 0.5, but values between 0 and 1 is valid
// power default value is 2
float half_lambert(float3 light_direction, float3 normal_ws, float wrap, float power) {
    float n_dot_l = dot(normal_ws, light_direction);
    return saturate(pow(abs(n_dot_l * wrap + (1 - wrap)), power));
}

#endif
