#ifndef SHADERKIT_BASIC_FRESNEL_HLSL
#define SHADERKIT_BASIC_FRESNEL_HLSL

// view_direction_ws can be obtained with:
// GetWorldSpaceNormalizeViewDir(varyings.position_ws);

float fresnel(float3 normal_ws, float3 view_direction_ws, float power) {
    float product = dot(normalize(normal_ws), normalize(view_direction_ws));
    return pow(1.0 - saturate(product), power);
}

#endif
