#ifndef SHADERKIT_BASIC_PHONG_HLSL
#define SHADERKIT_BASIC_PHONG_HLSL

float phong(
    float3 light_direction_ws,
    float3 normal_ws,
    float3 view_direction_ws,
    float shininess
) {
    float result = max(dot(view_direction_ws, reflect(-light_direction_ws, normal_ws)), 0);
    return pow(result, shininess);
}

#endif
