#ifndef SHADERKIT_BASIC_MINNAERT_HLSL
#define SHADERKIT_BASIC_MINNAERT_HLSL

float minnaert(
    float3 light_direction,
    float3 normal_ws,
    float3 view_direction_ws,
    float roughness
) {
    float n_dot_l = saturate(dot(normal_ws, light_direction));
    float n_dot_v = saturate(dot(normal_ws, view_direction_ws));

    return saturate(n_dot_l * pow(n_dot_l * n_dot_v, roughness));
}

#endif
