#ifndef SHADERKIT_BASIC_OREN_NAYAR_HLSL
#define SHADERKIT_BASIC_OREN_NAYAR_HLSL

// source: https://garykeen27.wixsite.com/portfolio/oren-nayar-shading

// approximation of oren-nayar brdf, has more accurate light distribution than lambert
// roughness is normalized value where 0 equals lambert shading, and 1 equals oren-nayar
float oren_nayar(
    float3 light_direction,
    float3 normal_ws,
    float3 view_direction_ws,
    float roughness
) {
    float n_dot_l = saturate(dot(normal_ws, light_direction));
    float angle_l_n = acos(n_dot_l);

    float n_dot_v = saturate(dot(normal_ws, view_direction_ws));
    float angle_v_n = acos(n_dot_v);

    float alpha = max(angle_v_n, angle_l_n);
    float beta = min(angle_v_n, angle_l_n);
    float gamma = cos(angle_v_n - angle_l_n);

    float roughness_squared = roughness * roughness;

    float A = 1 - 0.5 * (roughness_squared / (roughness_squared + 0.57));
    float B = 0.45 * (roughness_squared / (roughness_squared + 0.09));
    float C = sin(alpha) * tan(beta);

    return n_dot_l * (A + (B * max(0, gamma) * C));
}


#endif
