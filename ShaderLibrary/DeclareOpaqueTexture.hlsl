#ifndef SHADERKIT_DECLARE_OPAQUE_TEXTURE_HLSL
#define SHADERKIT_DECLARE_OPAQUE_TEXTURE_HLSL

// straight up ripped from
// Packages/com.unity.shadergraph/Editor/Generation/Targets/BuiltIn/ShaderLibrary/DeclareOpaqueTexture.hlsl

TEXTURE2D_X(_CameraOpaqueTexture); SAMPLER(sampler_CameraOpaqueTexture);

float3 SampleSceneColor(float2 uv) {
    return SAMPLE_TEXTURE2D_X(_CameraOpaqueTexture, sampler_CameraOpaqueTexture, UnityStereoTransformScreenSpaceTex(uv)).rgb;
}

float3 LoadSceneColor(uint2 uv) {
    return LOAD_TEXTURE2D_X(_CameraOpaqueTexture, uv).rgb;
}

#endif
