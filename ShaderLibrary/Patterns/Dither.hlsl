#ifndef SHADERKIT_PATTERNS_DITHER_HLSL
#define SHADERKIT_PATTERNS_DITHER_HLSL

// https://en.wikipedia.org/wiki/Ordered_dithering

static float BAYER_2x2[4] = {
    0.2, 0.6,
    0.8, 0.4
};

static float BAYER_4x4[16] = {
    0.0588235, 0.5294118, 0.1764706, 0.6470588,
    0.7647059, 0.2941176, 0.8823529, 0.4117647,
    0.2352941, 0.7058824, 0.1176471, 0.5882353,
    0.9411765, 0.4705882, 0.8235294, 0.3529412
};

static float BAYER_8x8[64] = {
    0.0153846, 0.5076923, 0.1384615, 0.6307692, 0.0461538, 0.5384615, 0.1692308, 0.6615385,
    0.7538462, 0.2615385, 0.8769231, 0.3846154, 0.7846154, 0.2923077, 0.9076923, 0.4153846,
    0.2000000, 0.6923077, 0.0769231, 0.5692308, 0.2307692, 0.7230769, 0.1076923, 0.6000000,
    0.9384615, 0.4461538, 0.8153846, 0.3230769, 0.9692308, 0.4769231, 0.8461538, 0.3538462,
    0.0615385, 0.5538462, 0.1846154, 0.6769231, 0.0307692, 0.5230769, 0.1538462, 0.6461538,
    0.8000000, 0.3076923, 0.9230769, 0.4307692, 0.7692308, 0.2769231, 0.8923077, 0.4000000,
    0.2461538, 0.7384615, 0.1230769, 0.6153846, 0.2153846, 0.7076923, 0.0923077, 0.5846154,
    0.9846154, 0.4923077, 0.8615385, 0.3692308, 0.9538462, 0.4615385, 0.8307692, 0.3384615
};

// the pattern is not multiplied by render scale
// this is because the resulting perceived opacity will appear more consistent if
// rendered using clip space position

// bayer matrix with 4 shades (2x2 matrix)
// returns true if the coordinate is visible.
bool dither_bayer_4(float input, float4 position_cs) {
    float2 uv = position_cs.xy;
    uint index = (uint(uv.x) % 2) * 2 + uint(uv.y) % 2;
    return input > BAYER_2x2[index];
}

// bayer matrix with 16 shades (4x4 matrix)
// returns true if the coordinate is visible.
bool dither_bayer_16(float input, float4 position_cs) {
    float2 uv = position_cs.xy;
    uint index = (uint(uv.x) % 4) * 4 + uint(uv.y) % 4;
    return input > BAYER_4x4[index];
}

// bayer matrix with 64 shades (8x8 matrix)
// returns true if the coordinate is visible.
bool dither_bayer_64(float input, float4 position_cs) {
    float2 uv = position_cs.xy;
    uint index = (uint(uv.x) % 8) * 8 + uint(uv.y) % 8;
    return input > BAYER_8x8[index];
}

#endif
