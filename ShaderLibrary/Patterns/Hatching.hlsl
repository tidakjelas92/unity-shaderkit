#ifndef SHADERKIT_PATTERNS_HATCHING_HLSL
#define SHADERKIT_PATTERNS_HATCHING_HLSL

#include "../Functions/Transformation.hlsl"

// radians is uv rotation in radians
// shading examples: lambert, phong, with value ranging from 0-1
float hatching(float2 uv, float radians, float scale, float shading) {
    float result = rotate(uv, radians).r;
    result *= scale;
    result = frac(result);
    result = step(result, shading);

    return result;
}

#endif
