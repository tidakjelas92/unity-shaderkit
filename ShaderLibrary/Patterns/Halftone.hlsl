#ifndef SHADERKIT_PATTERNS_HALFTONE_HLSL
#define SHADERKIT_PATTERNS_HALFTONE_HLSL

// taken from:
// https://www.ronja-tutorials.com/post/040-halftone-shading/
float halftone(float2 uv, float scale, float shading, float smoothness) {
    float2 actualUv = uv * scale;
    float2 gridUv = frac(actualUv);

    // explanation:
    // https://www.ronja-tutorials.com/post/046-fwidth/
    float dist = distance(float2(0.5, 0.5), gridUv);
    float deviation = fwidth(dist) * smoothness;

    // values close to 0 generates white spots on shaded areas
    // cutoff at 0.01 to clean it
    shading = shading < 0.01 ? -1 : shading;
    float result = smoothstep(dist - deviation, dist + deviation, shading);

    return result;
}

#endif
