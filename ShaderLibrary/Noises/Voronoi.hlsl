#ifndef SHADERKIT_NOISES_VORONOI_HLSL
#define SHADERKIT_NOISES_VORONOI_HLSL

// adapted from shader graph generated code

float2 generateVoronoiNoise(float2 input, float angleOffset) {
    float2x2 m = float2x2(15.27, 47.63, 99.41, 89.98);
    input = frac(sin(mul(input, m)) * 46839.32);
    return float2(sin(input.y * angleOffset) * 0.5 + 0.5, cos(input.x * angleOffset) * 0.5 + 0.5);
}

void voronoi(float2 uv, float scale, float angleOffset, out float result, out float cell) {
    float2 actualUv = uv * scale;
    float2 gridCoord = floor(actualUv);
    float2 gridUv = frac(actualUv) - 0.5;

    result = 100;
    for (float i = -1; i <= 1; i++) {
        for (float j = -1; j <= 1; j++) {
            float2 coord = float2(i, j);
            float2 offset = generateVoronoiNoise(gridCoord + coord, angleOffset);
            float2 pointPosition = coord + offset;

            float dist = distance(pointPosition, gridUv);
            if (dist < result) {
                result = dist;
                cell = offset.x;
            }
        }
    }
}

#endif
