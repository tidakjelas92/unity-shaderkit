#ifndef SHADERKIT_TRANSFORMATION_HLSL
#define SHADERKIT_TRANSFORMATION_HLSL

// taken from shader graph generated code
float2 rotate(float2 uv, float radians) {
    float s = sin(radians);
    float c = cos(radians);

    float2x2 rmatrix = float2x2(c, -s, s, c);
    return mul(uv, rmatrix);
}

#endif
