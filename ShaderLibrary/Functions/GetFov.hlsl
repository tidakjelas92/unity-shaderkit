#ifndef SHADERKIT_FUNCTIONS_GET_FOV_HLSL
#define SHADERKIT_FUNCTIONS_GET_FOV_HLSL

// From nilocat's anime shader
// If your project has a faster way to get camera fov in shader, you can replace this slow function to your method.
// For example, you write cmd.SetGlobalFloat("_CurrentCameraFOV",cameraFOV) using a new RendererFeature in C#.
// this is convenient but slower
// source: https://answers.unity.com/questions/770838/how-can-i-extract-the-fov-information-from-the-pro.html

float get_fov() {
    float t = unity_CameraProjection._m11;
    float rad_to_deg = 180 / PI;
    float fov = atan(1.0f / t) * 2.0 * rad_to_deg;
    return fov;
}

#endif
