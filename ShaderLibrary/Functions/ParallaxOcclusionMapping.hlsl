#ifndef SHADERKIT_FUNCTIONS_PARALLAX_OCCLUSION_MAPPING_HLSL
#define SHADERKIT_FUNCTIONS_PARALLAX_OCCLUSION_MAPPING_HLSL

#include "GetViewDirectionTS.hlsl"

// this function must be defined later.
float sample_height(float2 uv);

// view_direction_ws can be obtained with:
// half3 view_direction_ws = GetWorldSpaceNormalizeViewDir(varyings.position_ws);

float2 parallax_occlusion_mapping(
    float2 uv,
    float4 tangentWS,
    float3 normalWS,
    half3 viewDirectionWS,
    float heightScale
) {
    const float minLayers = 8.0;
    const float maxLayers = 32.0;
    half3 viewDirectionTS = get_view_direction_ts(tangentWS, normalWS, viewDirectionWS);
    float layers = lerp(maxLayers, minLayers, max(dot(float3(0, 0, 1), viewDirectionTS), 0));

    float layerDepth = 1.0 / layers;
    float currentLayerDepth = 0;

    float2 P = viewDirectionTS.xy * heightScale;
    float2 deltaUv = P / layers;

    float2 currentUv = uv;
		float currentDepthMapValue = sampleHeight(currentUv);

    float i = 0;
    while (i < layers && currentLayerDepth < currentDepthMapValue) {
        currentUv -= deltaUv;
				currentDepthMapValue = sampleHeight(currentUv);
        currentLayerDepth += layerDepth;
        i += 1;
    }

    float2 previousUv = currentUv + deltaUv;
    float afterDepth = currentDepthMapValue - currentLayerDepth;
		float beforeDepth = sampleHeight(previousUv) - currentLayerDepth + layerDepth;

    float weight = afterDepth / (afterDepth - beforeDepth);
    float2 finalUv = previousUv * weight + currentUv * (1.0 - weight);

    return finalUv;
}

#endif
