#ifndef SHADERKIT_FUNCTIONS_POLAR_COORDINATES_HLSL
#define SHADERKIT_FUNCTIONS_POLAR_COORDINATES_HLSL

float2 to_polar_coordinates(float2 uv, float2 center, float2 scale, float offset) {
    float2 centered_uv = uv - center;
    float angle = atan2(centered_uv.y, centered_uv.x) + offset;
    float distance = length(centered_uv);

    return float2(distance, angle / TWO_PI + 0.5) * scale;
}

#endif
