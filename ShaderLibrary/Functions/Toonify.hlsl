#ifndef SHADERKIT_FUNCTIONS_TOONIFY_HLSL
#define SHADERKIT_FUNCTIONS_TOONIFY_HLSL

float toonify(float value, float threshold, float smoothness) {
    return smoothstep(
        threshold - smoothness / 2,
        threshold + smoothness / 2,
        value
    );
}

#endif
