#ifndef SHADERKIT_FUNCTIONS_DEBUG_HLSL
#define SHADERKIT_FUNCTIONS_DEBUG_HLSL

// some defines for quick debugging

#define DEBUG_1(x) return half4(x, x, x, 1)
#define DEBUG_2(x) return half4(x, 0, 1)
#define DEBUG_3(x) return half4(x, 1)

#endif
