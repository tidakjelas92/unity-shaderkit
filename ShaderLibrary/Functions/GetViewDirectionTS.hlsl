#ifndef SHADERKIT_FUNCTIONS_GET_VIEW_DIRECTION_TS_HLSL
#define SHADERKIT_FUNCTIONS_GET_VIEW_DIRECTION_TS_HLSL

half3x3 get_tangent_matrix(half4 tangent_ws, half3 normal_ws) {
    // must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
    half3 unnormalized_normal_ws = normal_ws;
    const half renorm_factor = 1.0 / length(unnormalized_normal_ws);

    // use bitangent on the fly like in hdrp
    // IMPORTANT! If we ever support Flip on double sided materials ensure bitangent and tangent are NOT flipped.
    half cross_sign = (tangent_ws.w > 0.0 ? 1.0 : -1.0); // we do not need to multiple GetOddNegativeScale() here, as it is done in vertex shader
    half3 bitang = cross_sign * cross(normal_ws.xyz, tangent_ws.xyz);

    half3 WorldSpaceNormal = renorm_factor * normal_ws.xyz;       // we want a unit length Normal Vector node in shader graph

    // to preserve mikktspace compliance we use same scale renormFactor as was used on the normal.
    // This is explained in section 2.2 in "surface gradient based bump mapping framework"
    half3 WorldSpaceTangent = renorm_factor * tangent_ws.xyz;
    half3 WorldSpaceBiTangent = renorm_factor * bitang;

    return half3x3(WorldSpaceTangent, WorldSpaceBiTangent, WorldSpaceNormal);
}

half3 get_view_direction_ts(
    half4 tangent_ws,
    half3 normal_ws,
    half3 view_direction_ws
) {
    half3x3 tangent_matrix = get_tangent_matrix(tangent_ws, normal_ws);
    return mul(tangent_matrix, view_direction_ws);
}

#endif
