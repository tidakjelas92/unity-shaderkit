#ifndef SHADERKIT_FUNCTIONS_EASINGS_HLSL
#define SHADERKIT_FUNCTIONS_EASINGS_HLSL

// PI definition here, but no need to include
// #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Macros.hlsl"

// all functions adapted from easings.net
// credits to : Andrey Sitnik and Ivan Solovev

// all input values are expected to be normalized
// all return values, in turn, are normalized too.

float ease_in_sine(float value) {
    return 1 - cos((value * PI) / 2);
}

float ease_out_sine(float value) {
    return sin((value * PI) / 2);
}

float ease_in_out_sine(float value) {
    return -(cos(PI * value) - 1) / 2;
}

float ease_in_quad(float value) {
    return value * value;
}

float ease_out_quad(float value) {
    return 1 - (1 - value) * (1 - value);
}

float ease_in_out_quad(float value) {
    return value < 0.5
        ? 2 * value * value
        : 1 - pow(-2 * value + 2, 2) / 2;
}

float ease_in_cubic(float value) {
    return value * value * value;
}

float ease_out_cubic(float value) {
    return 1 - pow(1 - value, 3);
}

float ease_in_out_cubic(float value) {
    return value < 0.5
        ? 4 * value * value * value
        : 1 - pow(-2 * value + 2, 3) / 2;
}

float ease_in_quart(float value) {
    return value * value * value;
}

float ease_out_quart(float value) {
    return 1 - pow(1 - value, 4);
}

float ease_in_out_quart(float value) {
    return value < 0.5
        ? 8 * value * value * value * value
        : 1 - pow(-2 * value + 2, 4) / 2;
}

float ease_in_quint(float value) {
    return value * value * value * value * value;
}

float ease_out_quint(float value) {
    return 1 - pow(1 - value, 5);
}

float ease_in_out_quint(float value) {
    return value < 0.5
        ? 16 * value * value * value * value * value
        : 1 - pow(-2 * value + 2, 5) / 2;
}

float ease_in_expo(float value) {
    return value == 0
        ? 0
        : pow(2, 10 * value - 10);
}

float ease_out_expo(float value) {
    return value == 1
        ? 1
        : 1 - pow(2, -10 * value);
}

float ease_in_out_expo(float value) {
    return value == 0
        ? 0
        : value == 1
            ? 1
            : value < 0.5
                ? pow(2, 20 * value - 10) / 2
                : (2 - pow(2, -20 * value + 10)) / 2;
}

float ease_in_circ(float value) {
    return 1 - sqrt(1 - pow(value, 2));
}

float ease_out_circ(float value) {
    return sqrt(1 - pow(value - 1, 2));
}

float ease_in_out_circ(float value) {
    return value < 0.5
        ? (1 - sqrt(1 - pow(2 * value, 2))) / 2
        : (sqrt(1 - pow(-2 * value + 2, 2)) + 1) / 2;
}

float ease_in_back(float value) {
    const float c1 = 1.70158;
    const float c3 = c1 + 1;

    return c3 * value * value * value - c1 * value * value;
}

float ease_out_back(float value) {
    const float c1 = 1.70158;
    const float c3 = c1 + 1;

    return 1 + c3 * pow(value - 1, 3) + c1 * pow(value - 1, 2);
}

float ease_in_out_back(float value) {
    const float c1 = 1.70158;
    const float c2 = c1 * 1.525;

    return value < 0.5
        ? (pow(2 * value, 2) * ((c2 + 1) * 2 * value - c2)) / 2
        : (pow(2 * value - 2, 2) * ((c2 + 1) * (value * 2 - 2) + c2) + 2) / 2;
}

float ease_in_elastic(float value) {
    const float c4 = (2 * PI) / 3;

    return value == 0
        ? 0
        : value == 1
            ? 1
            : -pow(2, 10 * value - 10) * sin((value * 10 - 10.75) * c4);
}

float ease_out_elastic(float value) {
    const float c4 = (2 * PI) / 3;

    return value == 0
        ? 0
        : value == 1
            ? 1
            : pow(2, -10 * value) * sin((value * 10 - 0.75) * c4) + 1;
}

float ease_in_out_elastic(float value) {
    const float c5 = (2 * PI) / 4.5;

    return value == 0
        ? 0
        : value == 1
            ? 1
            : value < 0.5
                ? -(pow(2, 20 * value - 10) * sin(20 * value - 11.125) * c5) / 2
                : (pow(2, -20 * value + 10) * sin(20 * value - 11.125) * c5) / 2 + 1;
}

float ease_out_bounce(float value) {
    const float n1 = 7.5625;
    const float d1 = 2.75;

    if (value < 1 / d1) {
        return n1 * value * value;
    }
    if (value < 2 / d1) {
        float value2 = value - 1.5 / d1;
        return n1 * value2 * value + 0.75;
    }
    if (value < 2.5 / d1) {
        float value2 = value - 2.25 / d1;
        return n1 * value2 * value + 0.9375;
    }

    float value2 = value - 2.625 / d1;
    return n1 * value2 * value + 0.984375;
}

float ease_in_bounce(float value) {
    return 1 - ease_out_bounce(1 - value);
}

float ease_in_out_bounce(float value) {
    return value < 0.5
        ? (1 - ease_out_bounce(1 - 2 * value)) / 2
        : (1 + ease_out_bounce(2 * value - 1)) / 2;
}

#endif
