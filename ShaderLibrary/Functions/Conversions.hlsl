#ifndef SHADERKIT_FUNCTIONS_CONVERSIONS_HLSL
#define SHADERKIT_FUNCTIONS_CONVERSIONS_HLSL

// from alexander ameye
// source: https://ameye.dev/notes/stylized-water-shader/
float3 rgb_to_hsv(float3 color) {
    float4 k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    float4 p = lerp(
        float4(color.bg, k.wz),
        float4(color.gb, k.xy),
        step(color.b, color.g)
    );
    float4 q = lerp(
        float4(p.xyw, color.r),
        float4(color.r, p.yzx),
        step(p.x, color.r)
    );
    float d = q.x - min(q.w, q.y);
    float e = 1e-10;

    return float3(
        abs(q.z + (q.w - q.y)/(6.0 * d + e)),
        d / (q.x + e),
        q.x
    );
}

float3 hsv_to_rgb(float3 color) {
    float4 k = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    float3 p = abs(frac(color.xxx + k.xyz) * 6.0 - k.www);

    return color.z * lerp(k.xxx, saturate(p - k.xxx), color.y);
}

#endif
