#ifndef SHADERKIT_SQUARE_SCREEN_UV_HLSL
#define SHADERKIT_SQUARE_SCREEN_UV_HLSL

float2 get_square_screen_uv(float4 position_cs) {
    float shorter = min(_ScreenSize.x, _ScreenSize.y);
    return position_cs.xy / shorter;
}

#endif
