#ifndef SHADERKIT_FUNCTIONS_HSV_LERP_HLSL
#define SHADERKIT_FUNCTIONS_HSV_LERP_HLSL

#include "Conversions.hlsl"

// from alexander ameye
// source: https://ameye.dev/notes/stylized-water-shader/

float4 hsv_lerp(float4 a, float4 b, float t) {
    a.xyz = rgb_to_hsv(a.xyz);
    b.xyz = rgb_to_hsv(b.xyz);

    // used to lerp alpha, needs to remain unchanged
    float T = t;

    float hue;

    // hue difference
    float d = b.x - a.x;

    if(a.x > b.x) {
        float temp = b.x;
        b.x = a.x;
        a.x = temp;

        d = -d;
        t = 1-t;
    }

    if(d > 0.5) {
        a.x = a.x + 1;
        hue = (a.x + t * (b.x - a.x)) % 1;
    }

    if(d <= 0.5)
        hue = a.x + t * d;

    float saturation = a.y + t * (b.y - a.y);
    float value = a.z + t * (b.z - a.z);
    float alpha = a.w + t * (b.w - a.w);

    float3 rgb = hsv_to_rgb(float3(
        hue,
        saturation,
        value
    ));

    return float4(rgb, alpha);
}

#endif
