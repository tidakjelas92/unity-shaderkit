#ifndef SHADERKIT_OPAQUE_SHADOW_CASTER_HLSL
#define SHADERKIT_OPAQUE_SHADOW_CASTER_HLSL

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

struct Attributes {
    float3 position_os : POSITION;
    float3 normal_os : NORMAL;
};

struct Varyings {
    float4 position_cs : SV_POSITION;
};

float3 _LightDirection;

float4 get_shadow_caster_position_cs(float3 position_ws, float3 normal_ws) {
    float4 position_cs = TransformWorldToHClip(
        ApplyShadowBias(position_ws, normal_ws, _LightDirection)
    );

#if UNITY_REVERSED_Z
    position_cs.z = min(position_cs.z, UNITY_NEAR_CLIP_VALUE);
#else
    position_cs.z = max(position_cs.z, UNITY_NEAR_CLIP_VALUE);
#endif

    return position_cs;
}

Varyings vertex(Attributes attributes) {
    Varyings varyings;

    VertexPositionInputs vertex_position_inputs = GetVertexPositionInputs(attributes.position_os);
    VertexNormalInputs vertex_normal_inputs = GetVertexNormalInputs(attributes.normal_os);

    varyings.position_cs = get_shadow_caster_position_cs(
        vertex_position_inputs.positionWS, vertex_normal_inputs.normalWS
    );

    return varyings;
}

float4 fragment(Varyings varyings) : SV_TARGET {
    return float4(1, 1, 1, 1);
}

#endif
