#ifndef SHADERKIT_OPAQUE_DEPTH_NORMALS_HLSL
#define SHADERKIT_OPAQUE_DEPTH_NORMALS_HLSL

// this pass needs _NORMALMAP keyword enabled or disabled depending on the usage.
// this pass needs _ColorMap, _NormalMap, and _NormalStrength
// declared in the uniforms section

struct Attributes {
    float3 position_os : POSITION;
#if defined(_NORMALMAP)
    float2 uv : TEXCOORD0;
#endif
    float3 normal_os : NORMAL;
    float4 tangent_os : TANGENT;
};

struct Varyings {
    float4 position_cs : SV_POSITION;
#if defined(_NORMALMAP)
    float2 uv : TEXCOORD0;
#endif
    float3 normal_ws : TEXCOORD1;
#if defined(_NORMALMAP)
    float4 tangent_ws : TEXCOORD2;
#endif
};

Varyings vertex(Attributes attributes) {
    Varyings varyings;

    VertexPositionInputs vertex_position_inputs = GetVertexPositionInputs(attributes.position_os);
    VertexNormalInputs vertex_normal_inputs = GetVertexNormalInputs(attributes.normal_os, attributes.tangent_os);

    varyings.position_cs = vertex_position_inputs.positionCS;
#if defined(_NORMALMAP)
    varyings.uv = TRANSFORM_TEX(attributes.uv, _ColorMap);
    varyings.tangent_ws = float4(vertex_normal_inputs.tangentWS, attributes.tangent_os.w);
#endif
    varyings.normal_ws = vertex_normal_inputs.normalWS;

    return varyings;
}

float4 fragment(Varyings varyings) : SV_TARGET {
    float3 normal_ws = varyings.normal_ws;

#if defined(_NORMALMAP)
    float4 normal_sample = SAMPLE_TEXTURE2D(_NormalMap, sampler_NormalMap, varyings.uv);
    float3 normal_ts = (UnpackNormalScale(normal_sample, _NormalStrength) + 1) * 0.5;
    float3x3 tangent_to_world = CreateTangentToWorld(
        varyings.normal_ws, varyings.tangent_ws.xyz, varyings.tangent_ws.w
    );
    normal_ws = TransformTangentToWorld(
        normal_ts, tangent_to_world
    );
#endif

    return float4(normal_ws, 0);
}

#endif
