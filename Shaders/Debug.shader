Shader "ShaderKit/Debug" {
    Properties {
        [Enum(TidakJelas.ShaderKit.DebugData)] _DebugData("Debug data", Float) = 0
        [Toggle] _Frac("Frac", Float) = 0
        _Multiplier("Multiplier", Vector) = (1, 1, 1, 1)
        _Offset("Offset", Vector) = (0, 0, 0, 0)
    }

    SubShader {
        Tags {
            "RenderType" = "Opaque"
            "RenderPipeline" = "UniversalPipeline"
            "ShaderModel" = "4.5"
        }

        HLSLINCLUDE
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "DebugUniforms.hlsl"
        ENDHLSL

        Pass {
            Name "ForwardLit"
            Tags { "LightMode" = "UniversalForward" }

            ZWrite On
            Cull Back
            Blend One Zero

            HLSLPROGRAM
            #pragma vertex vertex
            #pragma fragment fragment

            #include "Debug.hlsl"
            ENDHLSL
        }
    }

    Fallback "Hidden/Universal Render Pipeline/FallbackError"
}
