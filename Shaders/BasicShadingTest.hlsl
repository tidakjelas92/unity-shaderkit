#ifndef SHADERKIT_BASIC_SHADING_TEST_HLSL
#define SHADERKIT_BASIC_SHADING_TEST_HLSL

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "../ShaderLibrary/Basic/Lambert.hlsl"
#include "../ShaderLibrary/Basic/Phong.hlsl"
#include "../ShaderLibrary/Basic/Blinn.hlsl"
#include "../ShaderLibrary/Basic/HalfLambert.hlsl"
#include "../ShaderLibrary/Basic/OrenNayar.hlsl"
#include "../ShaderLibrary/Basic/Minnaert.hlsl"

struct Attributes {
    float3 positionOS : POSITION;
    float3 normalOS : NORMAL;
    float4 tangentOS : TANGENT;
};

struct Varyings {
    float4 positionCS : SV_POSITION;
#if defined(_GOURAUD)
#if defined(_FLAT)
    nointerpolation float shading : TEXCOORD3;
#else
    float shading : TEXCOORD3;
#endif // _FLAT
#endif // _GOURAUD

#if defined(_FLAT)
    nointerpolation float3 normalOS : NORMAL;
    nointerpolation float4 tangentOS : TANGENT;
    nointerpolation float3 normalWS : TEXCOORD1;
    nointerpolation float3 positionWS : TEXCOORD2;
#else
    float3 normalOS : NORMAL;
    float4 tangentOS : TANGENT;
    float3 normalWS : TEXCOORD1;
    float3 positionWS : TEXCOORD2;
#endif // _FLAT
};

Varyings vertex(Attributes attributes) {
    Varyings varyings;

    VertexPositionInputs vertexPositionInputs = GetVertexPositionInputs(attributes.positionOS);
    VertexNormalInputs vertexNormalInputs = GetVertexNormalInputs(attributes.normalOS, attributes.tangentOS);

    varyings.positionCS = vertexPositionInputs.positionCS;

#if defined(_GOURAUD)
    Light light = GetMainLight();
    float3 normalWS = vertexNormalInputs.normalWS;
    float3 view_direction_ws = GetWorldSpaceNormalizeViewDir(vertexPositionInputs.positionWS);
    if (_Shading == 0) {
        varyings.shading = lambert(light.direction, normalWS);
    } else if (_Shading == 1) {
        varyings.shading = phong(
            light.direction, normalWS, view_direction_ws, _Shininess
        );
    } else if (_Shading == 2) {
        varyings.shading = blinn(
            light.direction, normalWS, view_direction_ws, _Shininess
        );
    } else if (_Shading == 3) {
        varyings.shading = half_lambert(
            light.direction, normalWS, _Wrap, _Power
        );
    } else if (_Shading == 4) {
        varyings.shading = oren_nayar(
            light.direction, normalWS, view_direction_ws, _Roughness
        );
    }
    } else if (_Shading == 5) {
        varyings.shading = minnaert(
            light.direction, normalWS, view_direction_ws, _Roughness
        );
    }
#endif
    varyings.normalOS = attributes.normalOS;
    varyings.tangentOS = attributes.tangentOS;
    varyings.normalWS = vertexNormalInputs.normalWS;
    varyings.positionWS = vertexPositionInputs.positionWS;

    return varyings;
}

float4 fragment(Varyings varyings) : SV_TARGET {
#if defined(_GOURAUD)
    return varyings.shading;
#else
    Light light = GetMainLight();
    float shading = 0;
    float3 normalWS = GetVertexNormalInputs(varyings.normalOS, varyings.tangentOS).normalWS;
    float3 view_direction_ws = GetWorldSpaceNormalizeViewDir(varyings.positionWS);

    if (_Shading == 0) {
        shading = lambert(light.direction, normalWS);
    } else if (_Shading == 1) {
        shading = phong(light.direction, normalWS, view_direction_ws, _Shininess);
    } else if (_Shading == 2) {
        shading = blinn(light.direction, normalWS, view_direction_ws, _Shininess);
    } else if (_Shading == 3) {
        shading = half_lambert(light.direction, normalWS, _Wrap, _Power);
    } else if (_Shading == 4) {
        shading = oren_nayar(
            light.direction, normalWS, view_direction_ws, _Roughness
        );
    } else if (_Shading == 5) {
        shading = minnaert(
            light.direction, normalWS, view_direction_ws, _Roughness
        );
    }

    return shading;
#endif
}

#endif
