Shader "ShaderKit/BasicShadingTest" {
    Properties {
        [Enum(TidakJelas.ShaderKit.ShadingType)] _Shading("Shading", Float) = 0
        [Toggle(_GOURAUD)] _Gouraud("Gouraud", Float) = 0
        [Toggle(_FLAT)] _Flat("Flat", Float) = 0
        _Shininess("Shininess", Float) = 8
        _Wrap("Wrap", Range(0, 1)) = 0.5
        _Power("Power", Float) = 2
        _Roughness("Roughness", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "RenderType" = "Opaque"
            "RenderPipeline" = "UniversalPipeline"
            "ShaderModel" = "4.5"
            "Queue" = "Geometry"
        }

        HLSLINCLUDE
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

        CBUFFER_START(UnityPerMaterial)
        float _Shading;
        float _Shininess;
        float _Wrap;
        float _Power;
        float _Roughness;
        CBUFFER_END
        ENDHLSL

        Pass {
            Name "ForwardLit"
            Tags { "LightMode" = "UniversalForward" }

            ZWrite On
            Cull Back
            Blend One Zero

            HLSLPROGRAM
            #pragma target 4.5
            #pragma vertex vertex
            #pragma fragment fragment

            // Additional lights
            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS

            // Shadows
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS _MAIN_LIGHT_SHADOWS_CASCADE _MAIN_LIGHT_SHADOWS_SCREEN
            #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile _ _SHADOWS_SOFT

            // Other
            #pragma multi_compile_fog
            #pragma multi_compile_instancing
            #pragma multi_compile _ DOTS_INSTANCING_ON
            #pragma multi_compile _ _SCREEN_SPACE_OCCLUSION

            #pragma shader_feature_local _ _GOURAUD
            #pragma shader_feature_local _ _FLAT

            #include "BasicShadingTest.hlsl"
            ENDHLSL
        }
    }

    Fallback "Hidden/Universal Render Pipeline/FallbackError"
}
