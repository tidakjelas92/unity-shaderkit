#ifndef SHADERKIT_DEBUG_HLSL
#define SHADERKIT_DEBUG_HLSL

struct Attributes {
    float3 positionOS : POSITION;
    float4 color : COLOR;
    float3 normalOS : NORMAL;
    float4 tangentOS : TANGENT;
    float2 uv : TEXCOORD0;
};

struct Varyings {
    float4 positionCS : SV_POSITION;
    float4 color : COLOR;
    float2 uv : TEXCOORD0;
    float3 positionOS : TEXCOORD1;
    float3 positionWS : TEXCOORD2;
    float3 positionVS : TEXCOORD3;
    float4 positionNDC : TEXCOORD4;
    float3 normalOS : TEXCOORD5;
    float3 normalWS : TEXCOORD6;
    float4 tangentOS : TEXCOORD7;
    real3 tangentWS : TEXCOORD8;
    real3 bitangentWS : TEXCOORD9;
};

Varyings vertex(Attributes attributes) {
    Varyings varyings;

    VertexPositionInputs vertexPositionInputs = GetVertexPositionInputs(attributes.positionOS);
    VertexNormalInputs vertexNormalInputs = GetVertexNormalInputs(attributes.normalOS, attributes.tangentOS);

    varyings.positionCS = vertexPositionInputs.positionCS;
    varyings.color = attributes.color;
    varyings.uv = attributes.uv;
    varyings.positionOS = attributes.positionOS;
    varyings.positionWS = vertexPositionInputs.positionWS;
    varyings.positionVS = vertexPositionInputs.positionVS;
    varyings.positionNDC = vertexPositionInputs.positionNDC;
    varyings.normalOS = attributes.normalOS;
    varyings.normalWS = vertexNormalInputs.normalWS;
    varyings.tangentOS = attributes.tangentOS;
    varyings.tangentWS = vertexNormalInputs.tangentWS;
    varyings.bitangentWS = vertexNormalInputs.bitangentWS;

    return varyings;
}

float4 fragment(Varyings varyings) : SV_TARGET {
    float3 finalColor = float3(1, 1, 1);
    float finalAlpha = 1;

    if (_DebugData == 0)
        finalColor = varyings.positionOS;
    else if (_DebugData == 1)
        finalColor = varyings.positionWS;
    else if (_DebugData == 2) {
        finalColor = varyings.positionCS.rgb;
        finalAlpha = varyings.positionCS.a;
    } else if (_DebugData == 3)
        finalColor = varyings.positionVS;
    else if (_DebugData == 4) {
        finalColor = varyings.positionNDC.rgb;
        finalAlpha = varyings.positionNDC.a;
    } else if (_DebugData == 5) {
        finalColor = varyings.color.rgb;
        finalAlpha = varyings.color.a;
    } else if (_DebugData == 6)
        finalColor = float3(varyings.uv.rg, 0);
    else if (_DebugData == 7)
        finalColor = varyings.normalOS;
    else if (_DebugData == 8)
        finalColor = varyings.normalWS;
    else if (_DebugData == 9) {
        finalColor = varyings.tangentOS.rgb;
        finalAlpha = varyings.tangentOS.a;
    } else if (_DebugData == 10)
        finalColor = varyings.tangentWS;
    else if (_DebugData == 11)
        finalColor = varyings.bitangentWS;


    finalColor = finalColor * _Multiplier.rgb + _Offset.rgb;
    finalAlpha = finalAlpha * _Multiplier.a + _Offset.a;

    if (_Frac)
        finalColor = frac(finalColor);

    return float4(finalColor, finalAlpha);
}

#endif
