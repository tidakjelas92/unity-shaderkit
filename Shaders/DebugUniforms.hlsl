#ifndef SHADERKIT_DEBUG_UNIFORMS_HLSL
#define SHADERKIT_DEBUG_UNIFORMS_HLSL

CBUFFER_START(UnityPerMaterial)

float _DebugData;
float4 _Multiplier;
float4 _Offset;
float _Frac;

CBUFFER_END

#endif
