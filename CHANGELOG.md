# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2023-06-07

### Added

- Basic shading
- Toonify

## [0.1.1] - 2023-05-31

### Changed

- Unity version to 2022.2.21f1
- URP version to 14.0.7

## [0.1.0] - 2022-03-31

### Added

- Dither
- Fresnel
- Parallax occlusion mapping
- Easings
- ShaderGUIHelper

[unreleased]: https://gitlab.com/tidakjelas92/unity-shaderkit/-/compare/v0.2.0...main
[0.1.0]: https://gitlab.com/tidakjelas92/unity-shaderkit/-/tags/v0.1.0
[0.1.1]: https://gitlab.com/tidakjelas92/unity-shaderkit/-/tags/v0.1.1
[0.2.0]: https://gitlab.com/tidakjelas92/unity-shaderkit/-/tags/v0.2.0
